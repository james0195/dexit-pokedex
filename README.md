## Getting Started

This is a repurposed project off of the original [dexit-pokedex](https://github.com/jalyna/oakdex-pokedex) project.
We have made some modefications to fit the needs of our WebRPG https://dexitrpg.net

### Javascript

Install the package:

```
$ npm install dexit-pokedex --save
```

Then you can use the library:

```js
dexit = require('dexit-pokedex');

const eevee = dexit.findPokemon('Eevee')
// returns data/pokemon/eevee.json
console.log(eevee.names.en); // Eeevee

const charmander = dexit.findPokemon(4)
// returns data/pokemon/charmander.json
console.log(charmander.names.en); // Charmander

const tackle = dexit.findMove('Tackle')
// returns data/move/tackle.json
console.log(m.names.en); // Tackle

const potion = dexit.findItem('Potion')
// returns data/item/potion.json
console.log(m.names.en); // Potion

const contrary = dexit.findAbility('Contrary')
// returns data/ability/contrary.json
console.log(a.names.en); // Contrary

const fairy = dexit.findType('Fairy')
// returns data/type/fairy.json
console.log(fairy.names.en); // Fairy

const water1 = dexit.findEggGroup('Water 1')
// returns data/egg_group/water_1.json
console.log(water1.names.en); // Water 1

const genVI = dexit.findGeneration('Generation VI')
// returns data/generation/6.json
console.log(genVI.names.en); // Generation VI

const bold = dexit.findNature('Bold')
// returns data/nature/bold.json
console.log(bold.names.en); // Bold

const allPokemon = dexit.allPokemon()
console.log(allPokemon.length); // 809

const allItems = dexit.allItems()
console.log(allItems.length); // 69, WIP

const darkPokemon = dexit.allPokemon({ type: 'Dark' })
console.log(darkPokemon.length); // 46

const humanLike = dexit.allPokemon({ egg_group: 'Human-Like' })
console.log(humanLike.length); // 52

const alola = dexit.allPokemon({ dex: 'alola' })
console.log(alola.length); // 302

const moves = dexit.allMoves({ type: 'Ground' })
console.log(moves.length); // 26
```


### Add custom Pokemon / Fakemon

You can add your own Pokémon and it will be checked if it is valid. You can and should add them as a batch as evolutions need to present at the same time.

Fakemon need to have a national_id that is greater than 10000.

Checkout [the fakemon examples](fixtures).

```js
fakemonStage1 = fs.readFileSync('./fixtures/my_fakemon1.json', 'utf8')
fakemonStage2 = fs.readFileSync('./fixtures/my_fakemon2.json', 'utf8')

dexit.importPokemon([fakemonStage1, fakemonStage2])

dexit.findPokemon('My Fakemon')
```


### Schemas

If you want to know what the structure of the given data is, checkout the following documentations:

- [Pokemon](doc/pokemon.md)
- [Item](doc/item.md)
- [Move](doc/move.md)
- [Ability](doc/ability.md)
- [Type](doc/type.md)
- [Egg Group](doc/egg_group.md)
- [Nature](doc/nature.md)
- [Generation](doc/generation.md)
- [Region](doc/region.md)


### Sprites

If you want also to include sprites in your pokedex, check out [dexit-pokedex-sprites](https://github.com/jalyna/dexit-pokedex-sprites).

## Contributing

I would be happy if you want to add your contribution to the project. In order to contribute, you just have to fork this repository.

Please respect the [Code of Conduct](//github.com/jalyna/dexit-pokedex/blob/master/CODE_OF_CONDUCT.md).

## License

MIT License. See the included MIT-LICENSE file.

## Credits

Logo Icon by [Roundicons Freebies](http://www.flaticon.com/authors/roundicons-freebies).

Original Project (oakdex-pokedex) by [jalyna](//github.com/jalyna).

Original Code Of Conduct by [jalyna](//github.com/jalyna).
