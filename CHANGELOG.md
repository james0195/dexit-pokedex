# Change Log
All notable changes to this project will be documented in this file.
Please keep to the changelog format described on [keepachangelog.com](http://keepachangelog.com).

## [0.0.0] - 2019-12-01

### Added
- Forked scripts from original oakdex-pokedex

## [0.0.1] - 2019-12-24

### Fixed
- Pokemon not found issues